package com.example.canvas_example.Model;

public class ShapesModel {
    int image;

    public ShapesModel(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
