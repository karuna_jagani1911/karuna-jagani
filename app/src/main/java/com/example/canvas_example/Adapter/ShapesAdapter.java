package com.example.canvas_example.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.canvas_example.MainActivity;
import com.example.canvas_example.Model.ShapesModel;
import com.example.canvas_example.R;

import java.util.ArrayList;

public class ShapesAdapter extends RecyclerView.Adapter<ShapesAdapter.ViewHolder> {

    private onShapeRecyclerViewItemClickListner mItemClickListener;

    public void setOnItemClickListener(onShapeRecyclerViewItemClickListner mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface onShapeRecyclerViewItemClickListner {
        void onShapeItemClickListener(View view, int position);
    }
    ArrayList<ShapesModel> shapeList=new ArrayList<>();

    public ShapesAdapter(MainActivity mainActivity, ArrayList<ShapesModel> shapeList) {
        this.shapeList=shapeList;
        this.mItemClickListener=(onShapeRecyclerViewItemClickListner) mainActivity;
    }

    @NonNull
    @Override
    public ShapesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shape_recyclerview, parent, false);
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShapesAdapter.ViewHolder holder, int position) {
     holder.shapeImage.setImageResource(shapeList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return shapeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView shapeImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            shapeImage=itemView.findViewById(R.id.image_shape);
            shapeImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onShapeItemClickListener(view, getAdapterPosition());
            }

        }
    }
}
