package com.example.canvas_example.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.canvas_example.MainActivity;
import com.example.canvas_example.Model.ColorModel;
import com.example.canvas_example.R;

import java.util.ArrayList;

public class ColorFilterAdapter extends RecyclerView.Adapter<ColorFilterAdapter.ViewHolder> {

    private onColorFilterRecyclerViewItemClickListner mItemClickListener;

    public void setOnItemClickListener(onColorFilterRecyclerViewItemClickListner mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface onColorFilterRecyclerViewItemClickListner {
        void onColorFilterItemClickListener(View view, int position);
    }


    ArrayList<Integer> ColorList=new ArrayList<>();

    public ColorFilterAdapter(MainActivity mainActivity, ArrayList<Integer> colorList) {
        this.ColorList=colorList;
        this.mItemClickListener=(onColorFilterRecyclerViewItemClickListner) mainActivity;
    }

    @NonNull
    @Override
    public ColorFilterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_colorfilter_recyclerview, parent, false);
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ColorFilterAdapter.ViewHolder holder, int position) {
      holder.tvColor.setBackgroundColor(ColorList.get(position));
    }

    @Override
    public int getItemCount() {
        return ColorList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvColor;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvColor=itemView.findViewById(R.id.tv_color);
            tvColor.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onColorFilterItemClickListener(view, getAdapterPosition());
            }
        }
    }
}
