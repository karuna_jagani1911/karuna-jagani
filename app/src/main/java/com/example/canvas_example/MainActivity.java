package com.example.canvas_example;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.canvas_example.Adapter.AdapterMenu;
import com.example.canvas_example.Adapter.ColorFilterAdapter;
import com.example.canvas_example.Adapter.ShapesAdapter;
import com.example.canvas_example.Model.MenuRecyclerviewModel;
import com.example.canvas_example.Model.ShapesModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.xiaopo.flying.sticker.BitmapStickerIcon;
import com.xiaopo.flying.sticker.DeleteIconEvent;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.FlipHorizontallyEvent;
import com.xiaopo.flying.sticker.Sticker;
import com.xiaopo.flying.sticker.StickerView;
import com.xiaopo.flying.sticker.TextSticker;
import com.xiaopo.flying.sticker.ZoomIconEvent;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.CAMERA;

public class MainActivity extends AppCompatActivity implements AdapterMenu.onRecyclerViewItemClickListener,ShapesAdapter.onShapeRecyclerViewItemClickListner,ColorFilterAdapter.onColorFilterRecyclerViewItemClickListner, View.OnClickListener {
    LinearLayout linearLayout;
    RecyclerView recyclerView,shapeRecyclerview,ColorFilterRecyclerview;
    AdapterMenu menuAdapter;
    ShapesAdapter shapesAdapter;
    ColorFilterAdapter colorFilterAdapter;
    ArrayList<MenuRecyclerviewModel> MenuList=new ArrayList<>();
    ArrayList<ShapesModel> shapeList=new ArrayList<>();
    ArrayList<Integer> ColorList=new ArrayList<>();

    private BottomSheetDialog bottomsheet;
    BottomSheetBehavior mBottomSheetBehavior;
    private TextView cancel;
    private Button btn_addtext;
    private EditText ed_user_wantadd_text;
    private LinearLayout linearLayout_gallery,linearLayout_camera;
    private ImageView shapeImage;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    private Bitmap bitmap;

    CoordinatorLayout coordinatorLayout;
    LinearLayout bottomSheetPersist;



    private static final String TAG = MainActivity.class.getSimpleName();
    private StickerView stickerView;
    private TextSticker sticker;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        stickerView = (StickerView) findViewById(R.id.sticker_view);
        recyclerView=findViewById(R.id.recyclerview);
        coordinatorLayout=findViewById(R.id.cordinatelayout);

        MenuList.add(new MenuRecyclerviewModel(R.drawable.ic_text,"Add Text"));
        MenuList.add(new MenuRecyclerviewModel(R.drawable.ic_addimage,"Add Image"));
        MenuList.add(new MenuRecyclerviewModel(R.drawable.ic_shaps,"Shapes"));
        MenuList.add(new MenuRecyclerviewModel(R.drawable.ic_graphics,"Graphics"));
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this)
        {
            @Override
            public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                // force height of viewHolder here, this will override layout_height from xml
                lp.width = getWidth() / 4;
                return true;
            }
        };

        mLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerView.setLayoutManager(mLayoutManager);
        menuAdapter = new AdapterMenu(this,MenuList);
        recyclerView.setAdapter(menuAdapter);
        recyclerView.setHasFixedSize(false);


        //shape list fill
        shapeList.add(new ShapesModel(R.drawable.circle));
        shapeList.add(new ShapesModel(R.drawable.ic_line_black_24dp));
        shapeList.add(new ShapesModel(R.drawable.triangle));
        shapeList.add(new ShapesModel(R.drawable.square));
        shapeList.add(new ShapesModel(R.drawable.ic_rectangle_black_24dp));

        //color list fill
        String colors[] = getApplicationContext().getResources().getStringArray(R.array.colors);
        for (int i = 0; i < colors.length; i++) {
            ColorList.add(Color.parseColor(colors[i]));
            Log.e("colorlist",ColorList.get(i).toString());

        }


        //currently you can config your own icons and icon event
        //the event you can custom
        BitmapStickerIcon deleteIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_close_white_18dp),
                BitmapStickerIcon.LEFT_TOP);
        deleteIcon.setIconEvent(new DeleteIconEvent());

        BitmapStickerIcon zoomIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_scale_white_18dp),
                BitmapStickerIcon.RIGHT_BOTOM);
        zoomIcon.setIconEvent(new ZoomIconEvent());

        BitmapStickerIcon flipIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_flip_white_18dp),
                BitmapStickerIcon.RIGHT_TOP);
        flipIcon.setIconEvent(new FlipHorizontallyEvent());

//        BitmapStickerIcon heartIcon =
//                new BitmapStickerIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp),
//                        BitmapStickerIcon.LEFT_BOTTOM);
//        heartIcon.setIconEvent(new HelloIconEvent());

        stickerView.setIcons(Arrays.asList(deleteIcon, zoomIcon, flipIcon));

        //default icon layout
        //stickerView.configDefaultIcons();

        stickerView.setBackgroundColor(Color.WHITE);
        stickerView.setLocked(false);
        stickerView.setConstrained(true);

        sticker = new TextSticker(this);

        sticker.setDrawable(ContextCompat.getDrawable(getApplicationContext(),
                R.drawable.sticker_transparent_background));
        sticker.setText("Hello, world!");
        sticker.setTextColor(Color.BLACK);
        sticker.setTextAlign(Layout.Alignment.ALIGN_CENTER);
        sticker.resizeText();

        stickerView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded");
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                //stickerView.removeAllSticker();
                if (sticker instanceof TextSticker) {
                    ((TextSticker) sticker).setTextColor(Color.RED);
                    stickerView.replace(sticker);
                    stickerView.invalidate();
                }
                Log.d(TAG, "onStickerClicked");
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown");
                behaviourBottomsheet();
                // CreateBottomSheetDialog(R.layout.bottomsheet_colorfilter,"ColorFilter");
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onDoubleTapped: double tap will be with two click");
                Toast.makeText(MainActivity.this, "click on listner double click", Toast.LENGTH_SHORT).show();
            }



        });

        CheckPermision();

//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED
//                || ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERM_RQST_CODE);
//        } else {
//            loadSticker();
//        }
    }

    private void CreateBottomSheetDialog(int layout, String type){
            switch (type)
            {
                case "AddText":
                    View viewAddText= LayoutInflater.from(this).inflate(layout,null);
                    btn_addtext =viewAddText.findViewById(R.id.btn_add);
                    ed_user_wantadd_text =viewAddText.findViewById(R.id.edittext_text);
                    cancel=viewAddText.findViewById(R.id.tv_cancel);
                    cancel.setOnClickListener(this);
                    btn_addtext.setOnClickListener(this);
                    bottomsheet=new BottomSheetDialog(this);
                    bottomsheet.setContentView(viewAddText);
                    break;
                case "AddImage":
                      View view= LayoutInflater.from(this).inflate(layout,null);
                      linearLayout_camera=view.findViewById(R.id.layout_camera);
                      linearLayout_gallery=view.findViewById(R.id.layout_gallery);
                    cancel=view.findViewById(R.id.tv_cancel);
                    cancel.setOnClickListener(this);
                      linearLayout_gallery.setOnClickListener(this);
                      linearLayout_camera.setOnClickListener(this);

                      bottomsheet=new BottomSheetDialog(this);
                      bottomsheet.setContentView(view);
                    break;
                case "AddShape":
                    View viewShapes= LayoutInflater.from(this).inflate(layout,null);
                    cancel=viewShapes.findViewById(R.id.tv_cancel);
                    cancel.setOnClickListener(this);

                    shapeRecyclerview=viewShapes.findViewById(R.id.shapes_recyclerview);
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),3);
                    shapeRecyclerview.setLayoutManager(gridLayoutManager);
                    shapesAdapter = new ShapesAdapter(this,shapeList);
                    shapeRecyclerview.setAdapter(shapesAdapter);
                    shapeRecyclerview.setHasFixedSize(false);

                    bottomsheet=new BottomSheetDialog(this);
                    bottomsheet.setContentView(viewShapes);
                    break;
              /*  case "ColorFilter":
                    View viewColorFilter= LayoutInflater.from(this).inflate(layout,null);
                    cancel=viewColorFilter.findViewById(R.id.tv_cancel);
                    cancel.setOnClickListener(this);

                    ColorFilterRecyclerview=viewColorFilter.findViewById(R.id.colorfilter_recyclerview);
                    GridLayoutManager gridLayoutManager1= new GridLayoutManager(getApplicationContext(),4);
                    ColorFilterRecyclerview.setLayoutManager(gridLayoutManager1);
                    colorFilterAdapter = new ColorFilterAdapter(this,ColorList);
                    ColorFilterRecyclerview.setAdapter(colorFilterAdapter);
                    ColorFilterRecyclerview.setHasFixedSize(false);

                    bottomsheet=new BottomSheetDialog(this);
                    bottomsheet.setContentView(viewColorFilter);
                    break;*/

            }

            bottomsheet.show();

    }

    @Override
    public void onItemClickListener(View view, int position) {

      switch (position)
      {
          case 0:
              Toast.makeText(getApplicationContext(),"text",Toast.LENGTH_LONG).show();
              CreateBottomSheetDialog(R.layout.bottomsheet_addtext_layout,"AddText");
              break;
          case 1:
              Toast.makeText(getApplicationContext(),"image",Toast.LENGTH_LONG).show();
              CreateBottomSheetDialog(R.layout.bottomsheet_addimage_layout,"AddImage");

              break;
          case 2:
              Toast.makeText(getApplicationContext(),"shapes",Toast.LENGTH_LONG).show();
              CreateBottomSheetDialog(R.layout.bottomsheet_addshapes_layout,"AddShape");
              break;
          case 3:
              Toast.makeText(getApplicationContext(),"Graphics",Toast.LENGTH_LONG).show();
              CreateBottomSheetDialog(R.layout.bottomsheet_addimage_layout,"Graphics");
              break;

      }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tv_cancelpersistant:
                bottomSheetPersist.setVisibility(View.GONE);
                break;
            case R.id.tv_cancel:
                if(bottomsheet.isShowing())
                bottomsheet.dismiss();
                break;
            case R.id.layout_camera:
                selectImageCamera();
                bottomsheet.dismiss();
                break;
            case R.id.layout_gallery:
                selectImageGallery();
                bottomsheet.dismiss();
                break;
            case R.id.btn_add:
               String text= ed_user_wantadd_text.getText().toString();
                AddText(view,text);
                Toast.makeText(this, "Add text", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onShapeItemClickListener(View view, int position) {
        switch (position)
        {
            case 0:
                AddShape(view,"Circle");
                bottomsheet.dismiss();
                break;
            case 1:
                AddShape(view,"Line");
                bottomsheet.dismiss();
                break;
            case 2:
                AddShape(view,"Triangle");
                bottomsheet.dismiss();
                break;
            case 3:
                AddShape(view,"Square");
                bottomsheet.dismiss();
                break;
            case 4:
                //Toast.makeText(this, "triangle", Toast.LENGTH_SHORT).show();
                AddShape(view,"Rect");
                break;

        }
    }

    private void loadSticker() {
        Drawable drawable =
                ContextCompat.getDrawable(this, R.drawable.haizewang_215);
        Drawable drawable1 =
                ContextCompat.getDrawable(this, R.drawable.haizewang_23);
        stickerView.addSticker(new DrawableSticker(drawable));
        stickerView.addSticker(new DrawableSticker(drawable1), Sticker.Position.BOTTOM | Sticker.Position.RIGHT);
//
        Drawable bubble = ContextCompat.getDrawable(this, R.drawable.bubble);
        stickerView.addSticker(
                new TextSticker(getApplicationContext())
                        .setDrawable(bubble)
                        .setText("Sticker\n")
                        .setMaxTextSize(14)
                        .resizeText()
                , Sticker.Position.TOP);
    }

    public void behaviourBottomsheet()
    {

        bottomSheetPersist =coordinatorLayout.findViewById(R.id.bottom_sheet22);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheetPersist);
        bottomSheetPersist.setVisibility(View.VISIBLE);
//       // mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        cancel=findViewById(R.id.tv_cancelpersistant);
        cancel.setOnClickListener(this);

        ColorFilterRecyclerview=bottomSheetPersist.findViewById(R.id.colorfilter_recyclerview);
        GridLayoutManager gridLayoutManager1= new GridLayoutManager(getApplicationContext(),4);
        ColorFilterRecyclerview.setLayoutManager(gridLayoutManager1);
        colorFilterAdapter = new ColorFilterAdapter(this,ColorList);
        ColorFilterRecyclerview.setAdapter(colorFilterAdapter);
        ColorFilterRecyclerview.setHasFixedSize(false);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == PERM_RQST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            loadSticker();
//        }
//    }

    public void AddText(View view, String text) {
        final TextSticker sticker = new TextSticker(this);
        sticker.setText(text);
        sticker.setTextColor(Color.BLUE);
        sticker.setTextAlign(Layout.Alignment.ALIGN_CENTER);
        sticker.resizeText();

        stickerView.addSticker(sticker);
    }

    public void AddShape(View view, String type) {
        Drawable drawable;
        switch (type)
        {
            case "Triangle":
                drawable =
                        ContextCompat.getDrawable(this, R.drawable.triangle);
                stickerView.addSticker(new DrawableSticker(drawable));
                break;
            case "Circle":
                drawable =
                        ContextCompat.getDrawable(this, R.drawable.circle);
                stickerView.addSticker(new DrawableSticker(drawable));
                break;
            case "Line":
                drawable =
                        ContextCompat.getDrawable(this, R.drawable.haizewang_215);
                stickerView.addSticker(new DrawableSticker(drawable));
                break;
            case "Square":
                drawable =
                        ContextCompat.getDrawable(this, R.drawable.square);
                stickerView.addSticker(new DrawableSticker(drawable));
                break;
            case "Rect":
                drawable =
                        ContextCompat.getDrawable(this, R.drawable.haizewang_215);
                stickerView.addSticker(new DrawableSticker(drawable));
                break;
        }

    }

    private void selectImageCamera() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {

                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);

            } else
                Toast.makeText(MainActivity.this, "Camera Permission error11", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void selectImageGallery() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(READ_EXTERNAL_STORAGE, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {

                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

            } else
                Toast.makeText(MainActivity.this, "Camera Permission error11", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
//                Uri selectedImage = data.getData();

                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Drawable d = new BitmapDrawable(getResources(), bitmap);

                stickerView.addSticker(new DrawableSticker(d));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            if(data != null) {
                Uri selectedImage = data.getData();
                try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                    Drawable d = new BitmapDrawable(getResources(), bitmap);

                    stickerView.addSticker(new DrawableSticker(d));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void editSticker(){
        Toast.makeText(this, "on click", Toast.LENGTH_SHORT).show();
    }

    public void CheckPermision() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                &&  ContextCompat.checkSelfPermission(this,CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA}
                    , 1);
            return;

        } else {
            loadSticker();
            //run when permision already given by user(click button second time at that time)
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadSticker();

                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                } else {

                    loadSticker();
                    Toast toast = Toast.makeText(MainActivity.this,"Permission Denied.\n" +
                            "Grant permission to download the file.", Toast.LENGTH_LONG);
                    toast.show();
                    // Toast.makeText(getApplicationContext(), "Permission Denied please give permission for download", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    @Override
    public void onColorFilterItemClickListener(View view, int position) {
        bottomSheetPersist.setVisibility(View.GONE);
        stickerView.setColorFilter(stickerView.getCurrentSticker(),ColorList.get(position));
    }
}

