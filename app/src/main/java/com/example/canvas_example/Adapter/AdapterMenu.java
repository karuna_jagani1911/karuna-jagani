package com.example.canvas_example.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.canvas_example.MainActivity;
import com.example.canvas_example.Model.MenuRecyclerviewModel;
import com.example.canvas_example.R;

import java.util.ArrayList;



public class AdapterMenu extends RecyclerView.Adapter<AdapterMenu.ViewHolder> {
     private onRecyclerViewItemClickListener mItemClickListener;
    ArrayList<MenuRecyclerviewModel> menuList = new ArrayList();

    public AdapterMenu(Context context, ArrayList<MenuRecyclerviewModel> menuList) {
        this.menuList=menuList;
        this.mItemClickListener = (onRecyclerViewItemClickListener) context;
    }

    public void setOnItemClickListener(onRecyclerViewItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface onRecyclerViewItemClickListener {
        void onItemClickListener(View view, int position);
    }

    @NonNull
    @Override
    public AdapterMenu.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_menu_recyclerview, parent, false);
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      holder.ImageMenu.setImageResource(menuList.get(position).getImage());
      holder.NameMenu.setText(menuList.get(position).getName());


    }


    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ImageMenu;
        TextView NameMenu;
        ConstraintLayout ItemMainLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ImageMenu=itemView.findViewById(R.id.image_menu);
            NameMenu=itemView.findViewById(R.id.tv_menu_name);
            ItemMainLayout=itemView.findViewById(R.id.item_mainlayout);
            ItemMainLayout.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClickListener(view, getAdapterPosition());
            }
        }
    }
}
